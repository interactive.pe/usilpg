import webpack from 'webpack'

export default {
	/*
	** Nuxt rendering mode
	** See https://nuxtjs.org/api/configuration-mode
	*/
	server: {
			port: 8000, // default: 3000
			host: '0.0.0.0', // default: localhost,
			timing: true
		},
	mode: 'universal',
	/*
	** Nuxt target
	** See https://nuxtjs.org/api/configuration-target
	*/
	target: 'static',
	/*
	** Headers of the page
	** See https://nuxtjs.org/api/configuration-head
	*/
	head: {
		title: process.env.npm_package_name || 'USIL',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' }
		],
		script:[
			{ src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js' },
			{ src: 'js/acordeones.js' },
			{ src: 'js/menu.js' },
			{ src: 'js/graficos.js' }
		]
	},
	/*
	** Global CSS
	*/
	css: [
		"node_modules/sanitize.css/sanitize.css",
		"node_modules/sanitize.css/typography.css",
		"node_modules/sanitize.css/forms.css",
		"~stylus/general.styl",
		"~stylus/movil-test.styl",
		"~stylus/movil-escuela.styl",
		"~stylus/movil-programas.styl"
	],
	/*
	** Plugins to load before mounting the App
	** https://nuxtjs.org/guide/plugins
	*/
	plugins: [
	],
	/*
	** Auto import components
	** See https://nuxtjs.org/api/configuration-components
	*/
	components: true,
	/*
	** Nuxt.js dev-modules
	*/
	buildModules: [
	],
	/*
	** Nuxt.js modules
	*/
	modules: [
		// Doc: https://github.com/nuxt/content
		'@nuxt/content',
	],
	/*
	** Content module configuration
	** See https://content.nuxtjs.org/configuration
	*/
	content: {},
	router: {
		base: '/usil/diseno/'
		// base: '/'
	},

	/*
	** Build configuration
	** See https://nuxtjs.org/api/configuration-build/
	*/
	build: {
		// analyze: {
		// 	analyzerMode: "static"
		// },
		extractCSS: true,

		// optimization: {

		// },
		filenames: {
			app: "[name].js",
			chunk: "[name].js",
			css: "[name].css",
			img: "[path][name].[ext]"
		},
		friendlyErrors: true,
		// hardSource: true,
		optimizeCSS: true,
		parallel: false,
		publicPath: "/",
		vendor: ["jquery"],
		plugins: [
			new webpack.ProvidePlugin({
				// global modules
				"$": "jquery",
			})
		]
	}
}
