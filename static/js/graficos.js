var $circulares;
var circulares;


jQuery( document ).ready(function() {
	$circulares = jQuery(".grafico-circular");
	// circulares = document.getElementsByClassName("grafico-circular");
	circulares = document.querySelectorAll(".grafico-circular");

	//jQuery.each($circulares, inicializarCircular);
	console.log(circulares);
	circulares.forEach(inicializarCircular);


});

// function inicializarCircular( i, nodo ){
// 	const $nodo = jQuery(nodo);
// 	const $lista = $nodo.find("ul")

// 	const datos = [];

// 	// console.log($lista);
// 	$lista.find("li").each(function(){
// 		// console.log(this);
// 		datos.push({
// 			etiqueta: $(this).text(),
// 			valor: $(this).data("valor"),
// 		})
// 	});

// 	$lista.remove();

// 	// console.log(datos);

// 	// const $svg = jQuery("<svg>")
// 	// 	.attr("xmlns", "http://www.w3.org/2000/svg")
// 	// 	.attr("WIDTH", "300")
// 	// 	.attr("height", "300")
// 	// 	.attr("viewBox", "-150 -150 300 300")

// 	// datos.forEach(item => {
// 	// 	const $curva = jQuery("<path/>")
// 	// 		.attr("data-valor", item.valor)
// 	// 		.attr("d", "M 150 0 A 150 150 1 1 0 0 150")
// 	// 		.attr("stroke-width", "14")

// 	// 	$svg.append($curva);
// 	// });

// 	const svg = document.createElement("svg");
// 	svg.setAttribute("xmlns", "http://www.w3.org/2000/svg")
// 	svg.setAttribute("viewBox", "-150 -150 300 300")


// 	$nodo.append(svg);
// }

const ns = 'http://www.w3.org/2000/svg'

function inicializarCircular( nodo ){
	//console.log(nodo);
	const lista = nodo.querySelector("ul");
	const datos = [];
	lista.querySelectorAll("li")
		.forEach(item => {
			datos.push({
							etiqueta: item.textContent,
							valor: item.dataset.valor,
						})
		})

	// console.log(datos);
	// lista.remove();

	// const svg = nodo.querySelector("svg");
	const svg = document.createElementNS(ns, "svg");
	svg.setAttribute("xmlns", "http://www.w3.org/2000/svg")
	svg.setAttribute("viewBox", "-150 -150 300 300")

	const gradoARadian = Math.PI/180;
	datos.forEach((item, i) => {
		console.log(i);

		const r = 142 - (i * 20)

		const a = 270 * (item.valor * 0.01)
		console.log(a);

		// const x = 0
		// const y = r

		const x = r * Math.cos(-a*gradoARadian)
		const y = r * Math.sin(-a*gradoARadian)

		const curva = document.createElementNS(ns, "path")
		const c = (a>180)?1:0

		curva.setAttribute("d", `M ${r} 0 A ${r} ${r} 0 ${c} 0 ${x} ${y}`)
		curva.setAttribute("data-valor", item.valor)
		// curva.setAttribute("stroke-width", "14")
		// curva.setAttribute("stroke", "black")

		svg.appendChild(curva);
	});

	nodo.append(svg);
}
