
var $menu;
var $menuInterno;

jQuery( document ).ready(function() {

	const $abrir = jQuery("#abrir-menu");
	$menu = jQuery("#menu");
	const $cerrar = $menu.find(".cerrar");
	const $regresar = $menu.find(".regresar");

	$abrir.on("click tap", evt => {
		$menu.addClass("abierto");
	})
	$cerrar.on("click tap", evt => {
		$menu.removeClass("abierto");
		$menu.find(".sub-menu.activo").removeClass("activo");
		$menu.removeClass("interna");
		evt.preventDefault();
	})
	$regresar.on("click tap", evt => {
		$menu.find(".sub-menu.activo").removeClass("activo");
		$menu.removeClass("interna");
		evt.preventDefault();
	})

	var movil = window.matchMedia("(max-width: 768px)")
	navegacionMovil(movil)
	movil.addListener(navegacionMovil)

	jQuery(".encuentra .item").on("click tap", function(){
		jQuery(this).siblings().removeClass("activo");
		jQuery(this).siblings().removeClass("abierto");
		jQuery(this).addClass("activo");
	});

	jQuery(".encuentra .item .mas").on("click tap", function(){
		jQuery(this).parents(".encuentra .item").addClass("abierto");

	});


	$menuInterno = jQuery(".navegacion .selector");
	$menuInterno.find(".activo").on("click tap", function(){
		$menuInterno.toggleClass("abierto");
	});

	jQuery(".buscar-xs .boton-encabezado").on("click tap", function(){
		jQuery(".buscar-xs").toggleClass("abierto");
	});

});

function navegacionMovil(query) {
	if (query.matches) {
		$menu.find("nav > ul > .menu-item-has-children > a").on("click tap", submenuClick)
		// activarNavegacion();
	} else {
		$menu.find("nav > ul > .menu-item-has-children > a").off("click tap", submenuClick)
		// desactivarNavegacion();
	}
}

function submenuClick( evt ){
	evt.preventDefault();

	const $enlace = jQuery(evt.currentTarget);
	console.debug($enlace);

	$enlace.next().addClass("activo");
	$menu.addClass("interna");
	$menu.scrollTop(0,0);

}


function activarNavegacion(){
	const $lista = $menuInterno.find("ul");
	const $cta = $menuInterno.find(".cta");

	$lista.remove();
}

function desactivarNavegacion(){

}
