
jQuery( document ).ready(function() {

	const $acordeones = jQuery(".acordeon");

	jQuery.each($acordeones, (i , acordeon) => {
		// console.debug(acordeon);
		const $items = jQuery(acordeon).find(".item");
		$items.removeClass("activo");
		$items.find(".encabezado").on("click tap", evt => {
			//jQuery(evt.currentTarget).parents(".item").toggleClass("activo");
			const $item = jQuery(evt.currentTarget).parents(".item");
			$item.toggleClass("activo");
			if ($item.hasClass("activo")) {
				$item.siblings().removeClass("activo");
			}
		});

		const $subItems = jQuery(acordeon).find(".subitem");
		$subItems.removeClass("activo");
		$subItems.find(".titulo").on("click tap", evt => {
			const $subitem = jQuery(evt.currentTarget).parents(".subitem");
			$subitem.toggleClass("activo");
			if ($subitem.hasClass("activo")) {
				$subitem.siblings().removeClass("activo");
			}
		});

	});

});
